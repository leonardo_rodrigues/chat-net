'use strict';

/**
 * @ngdoc function
 * @name chatNetApp.controller:ChatCtrl
 * @description # ChatCtrl Controller of the chatNetApp
 */
angular.module('chatNetApp')
  .controller("ChatCtrl", ["$scope", 
                           "$rootScope",
                           "$routeParams",
                           "ChatService", 
                           "UserService",
                           "$filter", function($scope, $rootScope, $routeParams, ChatService, UserService, $filter) {
	$scope.inputText = "";
	$scope.msgs = [];
	$scope.user = {};
	$scope.users = [];

	$scope.fechaMensagemErro = function() {
		$scope.msgError = "";
	};
	
	$scope.abreModalCarregar = function() {
		$("[id='pleaseWait']").modal();
	}
	
	$scope.fechaModalCarregar = function() {
		$("[id='pleaseWait']").modal('hide');
	}
	
	$scope.findUser = function() {
		UserService.findByEmail($routeParams.email)
		.success(function(data, status, headers) {
			$scope.user = data;
			$scope.listen();
			$scope.fechaModalCarregar();
		}).error(function(data, status, headers) {
			$scope.fechaModalCarregar();
			$scope.addMsg({
				erro: true,
				text: data, 
				user: "Servidor",
		        time: (new Date()).toUTCString()
				});
		});
	}
	
	$scope.submitMsg = function () {
		ChatService.chat({ 
			text: $scope.inputText, 
			user: $scope.user.name,
	        time: (new Date()).toUTCString()
	        }).error(function(data, status, headers) {
				$scope.msgError = data;
			});
	    $scope.inputText = "";
	};
	
	$scope.addMsg = function (msg) {
	    $scope.$apply(function () { 
	    	$scope.msgs.push(JSON.parse(msg.data)); 
	    });
	};
	
	$scope.setUsers = function (users) {
	    $scope.$apply(function () { 
	    	$scope.users = JSON.parse(users.data); 
	    });
	};
	
	$scope.listenMsg = function () {
	    $scope.chatFeed = new EventSource("app/chatFeed");
	    $scope.chatFeed.addEventListener("message", $scope.addMsg, false);
	};
	
	$scope.listenUsers = function () {
	    $scope.userFeed = new EventSource("app/userFeed/"+$scope.user.email);
	    $scope.userFeed.addEventListener("message", $scope.setUsers, false);
	};
	
	$scope.listen = function() {
		$scope.listenMsg();
		$scope.listenUsers();
	};
	
	$scope.findUser();
	$scope.abreModalCarregar();
}]);