'use strict';

/**
 * @ngdoc function
 * @name chatNetApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the chatNetApp
 */
angular.module('chatNetApp')
  .controller("LoginCtrl", ["Config",
                            "$location",
                            "$scope", 
                            "$rootScope", 
                            "UserService", 
                            function(Config, $location, $scope, $rootScope, UserService) {
		
	$scope.signIn = function(authResult) {
	  $scope.$apply(function() {
	    $scope.processAuth(authResult);
	  });
	};

	$scope.processAuth = function(authResult) {
	  if (authResult.status.signed_in) {
		$scope.saveAuthUserAndRedirect();
	  } else if (authResult.error) {
	     console.log('Erro:' + authResult.error);
	  }
	};
	
	$scope.saveAuthUserAndRedirect = function() {
		gapi.client.load('oauth2', 'v2', function() {
		  var request = gapi.client.oauth2.userinfo.get();
		  request.execute(function(resp) {
			  $scope.saveUserData(resp);
			  $location.path('/chat/'+resp.email);
		  });
		});
	};
	
	$scope.saveUserData = function(resp) {
		var signedUser = {};
		signedUser.email = resp.email;
		signedUser.name = resp.name;
		signedUser.picture = resp.picture;
		$rootScope.signedUser = signedUser;
		UserService.signin(signedUser);
	};

	$scope.renderSignIn = function() {
	  gapi.signin.render('myGsignin', {
	    'callback': $scope.signIn,
	    'clientid': Config.clientId,
	    'requestvisibleactions': Config.requestvisibleactions,
	    'scope': Config.scope,
	    'theme': 'dark',
	    'cookiepolicy': Config.cookiepolicy,
	    'accesstype': 'offline'
	  });
	};
	
	$scope.logout = function(user) {
		gapi.auth.signOut();
		UserService.logout(user);
		$location.path('/');
    };
	
	$scope.renderSignIn();
 }]);
