'use strict';

/**
 * @ngdoc overview
 * @name chatNetApp
 * @description # chatNetApp
 * 
 * Main module of the application.
 */
angular
  .module('chatNetApp', [
    'ngResource',
    'ngRoute',
    'ngSanitize'
  ])
  .constant("Config", {
      clientId: "400260120809-i03onalmkufdc3q5v365rukvohmljqvv.apps.googleusercontent.com",
      cookiepolicy: "single_host_origin",
      scope: "https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email",
      requestvisibleactions: 'http://schemas.google.com/AddActivity'
  })
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/chat/:email', {
        templateUrl: 'views/chat.html',
        controller: 'ChatCtrl',
        controllerAs: 'chat'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).run(function($rootScope, $location) {
	    $rootScope.$on("$routeChangeStart", function() {
	        if (!$rootScope.signedUser) {
	          $location.path('/');
	        }       
	    });
  });
