'use strict';

/**
 * @ngdoc service
 * @name chatNetApp.ChatService
 * @description
 * # ChatService
 * Service in the chatNetApp.
 */
angular.module('chatNetApp')
  .service('ChatService', ["$http", function($http) {
	this.chat = function(mensagem) {
		return $http.post("app/chat", mensagem);
	};
}]);
