'use strict';

/**
 * @ngdoc service
 * @name chatNetApp.UserService
 * @description
 * # UserService
 * Service in the chatNetApp.
 */
angular.module('chatNetApp')
   .service("UserService", ["$http", function($http) {
	this.findByEmail = function(email) {
		return $http.get("app/findUserByEmail/"+email);
	};
	this.logout = function(user) {
		return $http.post("app/logout", user);
	}
	this.signin = function(user) {
		return $http.post("app/signin", user);
	};
}]);
