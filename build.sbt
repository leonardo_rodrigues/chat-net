name := """chat-net"""

libraryDependencies ++= Seq(
	"ws.securesocial" %% "securesocial" % "2.1.4"
)

version := "1.0"

lazy val root = (project in file(".")).addPlugins(PlayJava)

