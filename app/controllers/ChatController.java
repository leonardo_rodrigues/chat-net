package controllers;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import play.libs.EventSource;
import play.mvc.Controller;
import play.mvc.Result;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ChatController extends Controller {

	private static List<EventSource> socketsChat = new CopyOnWriteArrayList<EventSource>();

	public static Result postMessage() {
		sendEventMessage(request().body().asJson());
		return ok();
	}
	
	public static Result postUserConnected(String user) {
		postUserMsgEvent(user, "O usuário "+user+" se conectou.");
		return ok();
	}
	
	public static Result postUserDisconnected(String user) {
		postUserMsgEvent(user, "O usuário "+user+" se desconectou.");
		return ok();
	}
	
	private static void postUserMsgEvent(String user, String msg) {
		JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
		ObjectNode objectNode = nodeFactory.objectNode();
		objectNode.put("info", true);
		objectNode.put("text", msg);
		objectNode.put("user", user);
		objectNode.put("time", new Date().toString());
		sendEventMessage(objectNode);
	}

	public static void sendEventMessage(JsonNode msg) {
		socketsChat.stream().forEach(es -> es.send(EventSource.Event.event(msg)));
	}

	public static Result chatFeed() {
		return ok(new EventSource() {
			@Override
			public void onConnected() {
				EventSource currentSocket = this;
				
				this.onDisconnected(() -> {
					socketsChat.remove(currentSocket);
				});
				
				socketsChat.add(currentSocket);
			}
		});
	}
}
