package controllers;

import models.User;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import service.UserFeedEventService;

public class UserController extends Controller {
	
	private static UserFeedEventService userFeedService = new UserFeedEventService();
	
	public static Result userSignin() {
		User user = Form.form(User.class).bindFromRequest(request()).get();
		userFeedService.addUserAndNotify(user);
		return created();
	}
	
	public static Result logout() {
		User user = Form.form(User.class).bindFromRequest(request()).get();
		userFeedService.removeUserAndNotify(user);
		return ok();
	}

	public static Result userFeed() {
		return ok(userFeedService.createUserEvent());
	}

}
