'use strict';

describe('Service: pingservice', function () {

  // load the service's module
  beforeEach(module('chatNetApp'));

  // instantiate service
  var pingservice;
  beforeEach(inject(function (_pingservice_) {
    pingservice = _pingservice_;
  }));

  it('should do something', function () {
    expect(!!pingservice).toBe(true);
  });

});
