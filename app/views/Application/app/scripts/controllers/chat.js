'use strict';

/**
 * @ngdoc function
 * @name chatNetApp.controller:ChatCtrl
 * @description # ChatCtrl Controller of the chatNetApp
 */
angular.module('chatNetApp')
  .controller("ChatCtrl", ["Config",
                           "$scope", 
                           "$rootScope", 
                           "$interval",
                           "ChatService", 
                           "UserService",
                           "PingEndPointService",
                           function(Config, $scope, $rootScope, $interval, ChatService, UserService, PingEndPointService) {
	$scope.inputText = "";
	$scope.msgs = [];
	$scope.users = [];
	$scope.msgErro = "";
	$scope.activityCounter = 0;
	$scope.intervalInativityCounter = {};
	$scope.chatFeed = {};
	$scope.userFeed = {};
	$scope.user = $rootScope.signedUser;

	$scope.fechaMensagemErro = function() {
		$scope.msgError = "";
	};
	
	$scope.submitMsg = function () {
		if(!$scope.inputText) {
			return;
		}
		ChatService.chat({ 
			text: $scope.inputText, 
			user: $scope.user.name,
	        time: (new Date()).toUTCString()
	        }).error(function(data) {
				$scope.msgError = data;
			});
	    $scope.inputText = "";
	};
	
	$scope.addMsg = function (msg) {
	    if(!$scope.$$phase) {
		    $scope.$apply(function () { 
		    	$scope.msgs.push(JSON.parse(msg.data)); 
		    });
		}
	    $scope.scrollWithMessage();
	};
	
	$scope.scrollWithMessage = function() {
	    var chatBox = document.getElementById("chatBox");
	    chatBox.scrollTop = chatBox.scrollHeight;
	}
	
	$scope.setUsers = function (users) {
	    if(!$scope.$$phase) {
		    $scope.$apply(function () { 
		    	if(users.data) {
		    		$scope.users = JSON.parse(users.data); 
		    	}
		    });
	    }
	};
	
	$scope.listen = function() {
		ChatService.listen($scope.addMsg);
		UserService.listen($scope.setUsers);
	};
	
	$scope.initCounter = function() {
		$scope.activityCounter = 0;
	};
	
	$scope.countInativity = function() {
		if($scope.activityCounter >= Config.inativityLimit) {
			$interval.cancel($scope.intervalInativityCounter);
			$scope.logout();
		}
		$scope.activityCounter += Config.inativitySecond;
	};
	
	$scope.logout = function() {
        $rootScope.$emit("CallLogout");
    };
    
    $scope.renderSignIn = function() {
        $rootScope.$emit("CallRenderSignIn");
    };
	
    $scope.signIn = function() {
        $rootScope.$emit("CallSignIn");
    };
    
	$scope.startInativityCounter = function() {
		$scope.intervalInativityCounter = $interval($scope.countInativity, Config.inativitySecond);  
	};
	
	$scope.$on('$destroy', function() {
		$interval.cancel($scope.intervalInativityCounter);
    });
	
	$scope.$on('$viewContentLoaded', function() {
		$scope.startInativityCounter();
	});
	
	$scope.listen();
    angular.element(window).on('mouseover', $scope.initCounter);
}]);
