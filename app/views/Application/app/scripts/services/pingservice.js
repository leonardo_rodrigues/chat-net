'use strict';

/**
 * @ngdoc service
 * @name chatNetApp.pingservice
 * @description
 * # pingservice
 * Factory in the chatNetApp.
 */
angular.module('chatNetApp')
  .factory('PingEndPointService', function($timeout, $http) {
      function PingService($timeout, $http) {
          var self = this;
          var lastTripTime = 0;
          
          self.onPingChanged = null;
          
          var testPing = function() {
              var start = (new Date()).getTime();
              $http.get('/')
                  .success(function() {
                      lastTripTime = (new Date()).getTime() - start;
                      if(self.onPingChanged !== null) {
                        self.onPingChanged(lastTripTime);
                      }
                      $timeout(testPing, 1000);
                  });
          };
          testPing();
      }
      
      return new PingService($timeout, $http);
  });
