'use strict';

/**
 * @ngdoc service
 * @name chatNetApp.ChatService
 * @description
 * # ChatService
 * Service in the chatNetApp.
 */
angular.module('chatNetApp')
  .service('ChatService', ["$http", function($http) {
	this.chatFeed = {};
	this.chat = function(mensagem) {
		return $http.post("app/chat", mensagem);
	};
	this.listen = function (callback) {
	    this.chatFeed = new EventSource("http://localhost:9000/app/chatFeed");
	    this.chatFeed.addEventListener("message", callback, false);
	};
}]);
