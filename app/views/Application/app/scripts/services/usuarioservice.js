'use strict';

/**
 * @ngdoc service
 * @name chatNetApp.UserService
 * @description
 * # UserService
 * Service in the chatNetApp.
 */
angular.module('chatNetApp')
   .service("UserService", ["$http", 
                            "$rootScope", 
                            function($http, $rootScope) {
	this.userFeed = {};
	this.logout = function() {
		var postData = $http.post("app/logout", $rootScope.signedUser);
		$rootScope.signedUser = undefined;
		return postData;
	};
	this.signin = function(user) {
		$rootScope.signedUser = user;
		return $http.post("app/signin", user);
	};
	this.listen = function(callback) {
	    this.userFeed = new EventSource("http://localhost:9000/app/userFeed");
	    this.userFeed.addEventListener("open", callback, false);
	    this.userFeed.addEventListener("message", callback, false);
	};
}]);
