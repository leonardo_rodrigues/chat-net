package service;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import models.User;
import play.libs.EventSource;
import play.libs.Json;
import controllers.ChatController;

public class UserFeedEventService {
	private Set<EventSource> connections = new CopyOnWriteArraySet<EventSource>();
	private Set<User> usuariosOnline = new CopyOnWriteArraySet<User>(); 

	public void addUserAndNotify(User user) {
		if(isUserValid(user)) {
			return;
		}
		usuariosOnline.add(user);
		ChatController.postUserConnected(user.getName());
		sendUserEvent();
	}

	public void removeUserAndNotify(User user) {
		if(isUserValid(user)) {
			return;
		}
		usuariosOnline.remove(user);
		ChatController.postUserDisconnected(user.getName());
		sendUserEvent();
	}

	private boolean isUserValid(User user) {
		return user == null || user.getName() == null;
	}

	public EventSource createUserEvent() {
		EventSource userEvent = new EventSource() {

			@Override
			public void onConnected() {
				this.onDisconnected(() -> {
					connections.remove(this);
				});
				connections.add(this);
			}
			
		};
		return userEvent;
	}

	private void sendUserEvent() {
		connections.stream().forEach(e -> e.send(EventSource.Event.event(Json.toJson(usuariosOnline))));
	}
}
