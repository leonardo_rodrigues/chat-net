package models;

import java.util.Date;

import play.data.validation.Constraints.Required;

public class Message {
	@Required
	private String text;
	private String user;
	private Date time;
	protected String getText() {
		return text;
	}
	protected void setText(String text) {
		this.text = text;
	}
	protected String getUser() {
		return user;
	}
	protected void setUser(String user) {
		this.user = user;
	}
	protected Date getTime() {
		return time;
	}
	protected void setTime(Date time) {
		this.time = time;
	}
	
}
